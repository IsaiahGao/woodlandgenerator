package projectz.listeners.events;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.ItemStack;

public final class BottleFillEvent extends PlayerEvent implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private Block block;
    private ItemStack from;
    private ItemStack to;
    
    public BottleFillEvent(Player player, ItemStack from, ItemStack to, Block block) {
        super(player);
        this.block = block;
        this.from = from;
        this.to = to;
    }

    @Override
	public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
    
    public ItemStack getItemFrom() {
    	return this.from;
    }
    
    public ItemStack getItemTo() {
    	return this.to;
    }
    
    public void setItemTo(ItemStack item) {
    	this.to = item;
    }
 
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public Block getBlock() {
        return this.block;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
	public boolean isCancelled() {
        return cancelled;
    }
}