package projectz.listeners.events;

import org.bukkit.entity.Fish;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.inventory.ItemStack;

public final class PlayerBucketFishEvent extends PlayerEvent implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private Fish entity;
    private ItemStack bucketFrom;
    private ItemStack bucketTo;
    
    public PlayerBucketFishEvent(Player player, Fish entity, ItemStack bucketFrom, ItemStack bucketTo) {
        super(player);
        this.entity = entity;
        this.bucketFrom = bucketFrom;
        this.bucketTo = bucketTo;
    }

    @Override
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
    
    public ItemStack getItemFrom() {
        return this.bucketFrom;
    }
    
    public ItemStack getItemTo() {
        return this.bucketTo;
    }
    
    public void setItemTo(ItemStack item) {
        this.bucketTo = item;
    }
 
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public Fish getFish() {
        return this.entity;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }
}