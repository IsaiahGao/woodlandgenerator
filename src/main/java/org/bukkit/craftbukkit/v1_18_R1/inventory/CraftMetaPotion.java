package org.bukkit.craftbukkit.v1_18_R1.inventory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.Validate;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.configuration.serialization.DelegateDeserialization;
import org.bukkit.craftbukkit.v1_18_R1.potion.CraftPotionUtil;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

@DelegateDeserialization(value = CraftMetaItem.SerializableMeta.class)
class CraftMetaPotion extends CraftMetaItem implements PotionMeta {
    static final CraftMetaItem.ItemMetaKey AMPLIFIER = new CraftMetaItem.ItemMetaKey("Amplifier", "amplifier");
    static final CraftMetaItem.ItemMetaKey AMBIENT = new CraftMetaItem.ItemMetaKey("Ambient", "ambient");
    static final CraftMetaItem.ItemMetaKey DURATION = new CraftMetaItem.ItemMetaKey("Duration", "duration");
    static final CraftMetaItem.ItemMetaKey SHOW_PARTICLES = new CraftMetaItem.ItemMetaKey("ShowParticles",
            "has-particles");
    static final CraftMetaItem.ItemMetaKey SHOW_ICON = new CraftMetaItem.ItemMetaKey("ShowIcon", "has-icon");
    static final CraftMetaItem.ItemMetaKey POTION_EFFECTS = new CraftMetaItem.ItemMetaKey("CustomPotionEffects",
            "custom-effects");
    static final CraftMetaItem.ItemMetaKey POTION_COLOR = new CraftMetaItem.ItemMetaKey("CustomPotionColor",
            "custom-color");
    static final CraftMetaItem.ItemMetaKey ID = new CraftMetaItem.ItemMetaKey("Id", "potion-id");
    static final CraftMetaItem.ItemMetaKey DEFAULT_POTION = new CraftMetaItem.ItemMetaKey("Potion", "potion-type");
    private PotionData type = new PotionData(PotionType.UNCRAFTABLE, false, false);
    private List<PotionEffect> customEffects;
    private Color color;
    
    private String customPotionTag; // MELON

    CraftMetaPotion(CraftMetaItem meta) {
        super(meta);
        if (!(meta instanceof CraftMetaPotion)) {
            return;
        }
        CraftMetaPotion potionMeta = (CraftMetaPotion) meta;
        this.type = potionMeta.type;
        this.color = potionMeta.color;
        if (potionMeta.hasCustomEffects()) {
            this.customEffects = new ArrayList<PotionEffect>(potionMeta.customEffects);
        }
    }

    CraftMetaPotion(NBTTagCompound tag) {
        super(tag);
        this.type = new PotionData(PotionType.UNCRAFTABLE, false, false);
        
        this.customPotionTag = tag.getString(CraftMetaPotion.DEFAULT_POTION.NBT); // MELON
        
        if (tag.hasKey(CraftMetaPotion.DEFAULT_POTION.NBT)) {
            this.type = CraftPotionUtil.toBukkit(tag.getString(CraftMetaPotion.DEFAULT_POTION.NBT));
        }
        if (tag.hasKey(CraftMetaPotion.POTION_COLOR.NBT)) {
            try {
                this.color = Color.fromRGB(tag.getInt(CraftMetaPotion.POTION_COLOR.NBT));
            } catch (IllegalArgumentException illegalArgumentException) {
                // empty catch block
            }
        }
        if (tag.hasKey(CraftMetaPotion.POTION_EFFECTS.NBT)) {
            NBTTagList list = tag.getList(CraftMetaPotion.POTION_EFFECTS.NBT, 10);
            int length = list.size();
            this.customEffects = new ArrayList<PotionEffect>(length);
            for (int i = 0; i < length; ++i) {
            	NBTTagCompound effect = list.getCompound(i);
                PotionEffectType type = PotionEffectType.getById(effect.getByte(CraftMetaPotion.ID.NBT));
                if (type == null)
                    continue;
                byte amp = effect.getByte(CraftMetaPotion.AMPLIFIER.NBT);
                int duration = effect.getInt(CraftMetaPotion.DURATION.NBT);
                boolean ambient = effect.getBoolean(CraftMetaPotion.AMBIENT.NBT);
                boolean particles = tag.hasKeyOfType(CraftMetaPotion.SHOW_PARTICLES.NBT, 1)
                        ? effect.getBoolean(CraftMetaPotion.SHOW_PARTICLES.NBT)
                        : true;
                boolean icon = tag.hasKeyOfType(CraftMetaPotion.SHOW_ICON.NBT, 1)
                        ? effect.getBoolean(CraftMetaPotion.SHOW_ICON.NBT)
                        : particles;
                this.customEffects.add(new PotionEffect(type, duration, amp, ambient, particles, icon));
            }
        }
    }

    CraftMetaPotion(Map<String, Object> map) {
        super(map);
        this.type = CraftPotionUtil.toBukkit(CraftMetaItem.SerializableMeta.getString(map,
                CraftMetaPotion.DEFAULT_POTION.BUKKIT, true));
        
        this.customPotionTag = SerializableMeta.getString(map, CraftMetaPotion.DEFAULT_POTION.NBT, true); // MELON
       
        Color color = CraftMetaItem.SerializableMeta.getObject(Color.class, map,
                (Object) CraftMetaPotion.POTION_COLOR.BUKKIT, true);
        if (color != null) {
            this.setColor(color);
        }
        Iterable<?> rawEffectList = CraftMetaItem.SerializableMeta.getObject(Iterable.class, map,
                (Object) CraftMetaPotion.POTION_EFFECTS.BUKKIT, true);
        if (rawEffectList == null) {
            return;
        }
        for (Object obj : rawEffectList) {
            if (!(obj instanceof PotionEffect)) {
                throw new IllegalArgumentException("Bad effect list.");
            }
            this.addCustomEffect((PotionEffect) obj, true);
        }
    }

    @Override
    void applyToItem(NBTTagCompound tag) {
        super.applyToItem(tag);
        
        tag.setString(CraftMetaPotion.DEFAULT_POTION.NBT, customPotionTag == null ? CraftPotionUtil.fromBukkit(this.type) : customPotionTag); // MELON
       
        if (this.hasColor()) {
            tag.setInt(CraftMetaPotion.POTION_COLOR.NBT, this.color.asRGB());
        }
        if (this.customEffects != null) {
            NBTTagList effectList = new NBTTagList();
            tag.set(CraftMetaPotion.POTION_EFFECTS.NBT, effectList);
            for (PotionEffect effect : this.customEffects) {
                NBTTagCompound effectData = new NBTTagCompound();
                effectData.setByte(CraftMetaPotion.ID.NBT, (byte) effect.getType().getId());
                effectData.setByte(CraftMetaPotion.AMPLIFIER.NBT, (byte) effect.getAmplifier());
                effectData.setInt(CraftMetaPotion.DURATION.NBT, effect.getDuration());
                effectData.setBoolean(CraftMetaPotion.AMBIENT.NBT, effect.isAmbient());
                effectData.setBoolean(CraftMetaPotion.SHOW_PARTICLES.NBT, effect.hasParticles());
                effectData.setBoolean(CraftMetaPotion.SHOW_ICON.NBT, effect.hasIcon());
                effectList.add(effectData);
            }
        }
    }

    @Override
    boolean isEmpty() {
        return super.isEmpty() && this.isPotionEmpty();
    }

    boolean isPotionEmpty() {
        return this.type.getType() == PotionType.UNCRAFTABLE && !this.hasCustomEffects() && !this.hasColor();
    }

    @Override
    boolean applicableTo(Material type) {
        switch (type) {
            case POTION :
            case SPLASH_POTION :
            case LINGERING_POTION :
            case TIPPED_ARROW : {
                return true;
            }
        }
        return false;
    }

    @Override
    public CraftMetaPotion clone() {
        CraftMetaPotion clone = (CraftMetaPotion) super.clone();
        clone.type = this.type;
        
        clone.customPotionTag = this.customPotionTag; // MELON
        
        if (this.customEffects != null) {
            clone.customEffects = new ArrayList<PotionEffect>(this.customEffects);
        }
        return clone;
    }

    @Override
    public void setBasePotionData(PotionData data) {
        Validate.notNull(data, "PotionData cannot be null");
        this.type = data;
    }

    @Override
    public PotionData getBasePotionData() {
        return this.type;
    }

    @Override
    public boolean hasCustomEffects() {
        return this.customEffects != null;
    }

    @Override
    public List<PotionEffect> getCustomEffects() {
        if (this.hasCustomEffects()) {
            return ImmutableList.copyOf(this.customEffects);
        }
        return ImmutableList.of();
    }

    @Override
    public boolean addCustomEffect(PotionEffect effect, boolean overwrite) {
        Validate.notNull(effect, "Potion effect must not be null");
        int index = this.indexOfEffect(effect.getType());
        if (index != -1) {
            if (overwrite) {
                PotionEffect old = this.customEffects.get(index);
                if (old.getAmplifier() == effect.getAmplifier() && old.getDuration() == effect.getDuration()
                        && old.isAmbient() == effect.isAmbient()) {
                    return false;
                }
                this.customEffects.set(index, effect);
                return true;
            }
            return false;
        }
        if (this.customEffects == null) {
            this.customEffects = new ArrayList<PotionEffect>();
        }
        this.customEffects.add(effect);
        return true;
    }

    @Override
    public boolean removeCustomEffect(PotionEffectType type) {
        Validate.notNull(type, "Potion effect type must not be null");
        if (!this.hasCustomEffects()) {
            return false;
        }
        boolean changed = false;
        Iterator<PotionEffect> iterator = this.customEffects.iterator();
        while (iterator.hasNext()) {
            PotionEffect effect = iterator.next();
            if (!type.equals(effect.getType()))
                continue;
            iterator.remove();
            changed = true;
        }
        if (this.customEffects.isEmpty()) {
            this.customEffects = null;
        }
        return changed;
    }

    @Override
    public boolean hasCustomEffect(PotionEffectType type) {
        Validate.notNull(type, "Potion effect type must not be null");
        return this.indexOfEffect(type) != -1;
    }

    @Override
    public boolean setMainEffect(PotionEffectType type) {
        Validate.notNull(type, "Potion effect type must not be null");
        int index = this.indexOfEffect(type);
        if (index == -1 || index == 0) {
            return false;
        }
        PotionEffect old = this.customEffects.get(0);
        this.customEffects.set(0, this.customEffects.get(index));
        this.customEffects.set(index, old);
        return true;
    }

    private int indexOfEffect(PotionEffectType type) {
        if (!this.hasCustomEffects()) {
            return -1;
        }
        for (int i = 0; i < this.customEffects.size(); ++i) {
            if (!this.customEffects.get(i).getType().equals(type))
                continue;
            return i;
        }
        return -1;
    }

    @Override
    public boolean clearCustomEffects() {
        boolean changed = this.hasCustomEffects();
        this.customEffects = null;
        return changed;
    }

    @Override
    public boolean hasColor() {
        return this.color != null;
    }

    @Override
    public Color getColor() {
        return this.color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    int applyHash() {
        int original;
        int hash = original = super.applyHash();
        if (this.type.getType() != PotionType.UNCRAFTABLE) {
            hash = 73 * hash + this.type.hashCode();
        }
        
        // MELON START
        if (this.customPotionTag != null) {
            hash = 73 * hash + this.customPotionTag.hashCode();
        }
        // MELON END
        
        if (this.hasColor()) {
            hash = 73 * hash + this.color.hashCode();
        }
        if (this.hasCustomEffects()) {
            hash = 73 * hash + this.customEffects.hashCode();
        }
        return original != hash ? CraftMetaPotion.class.hashCode() ^ hash : hash;
    }

    @Override
    public boolean equalsCommon(CraftMetaItem meta) {
        if (!super.equalsCommon(meta)) {
            return false;
        }
        if (meta instanceof CraftMetaPotion) {
            CraftMetaPotion that = (CraftMetaPotion) meta;
            // MELON START
            return (this.type.equals(that.type) || (this.customPotionTag != null && this.customPotionTag.equals(that.customPotionTag)))
            // MELON END
                    && (this.hasCustomEffects()
                            ? that.hasCustomEffects() && this.customEffects.equals(that.customEffects)
                            : !that.hasCustomEffects())
                    && (this.hasColor() ? that.hasColor() && this.color.equals(that.color) : !that.hasColor());
        }
        return true;
    }

    @Override
    boolean notUncommon(CraftMetaItem meta) {
        return super.notUncommon(meta) && (meta instanceof CraftMetaPotion || this.isPotionEmpty());
    }

    @Override
    ImmutableMap.Builder<String, Object> serialize(ImmutableMap.Builder<String, Object> builder) {
        super.serialize(builder);
        if (this.type.getType() != PotionType.UNCRAFTABLE) {
            builder.put(CraftMetaPotion.DEFAULT_POTION.BUKKIT,
                    CraftPotionUtil.fromBukkit(this.type));
        }
        if (this.hasColor()) {
            builder.put(CraftMetaPotion.POTION_COLOR.BUKKIT, this.getColor());
        }
        if (this.hasCustomEffects()) {
            builder.put(CraftMetaPotion.POTION_EFFECTS.BUKKIT,
                    ImmutableList.copyOf(this.customEffects));
        }
        
        // MELON START
        if (this.customPotionTag != null) {
            builder.put(CraftMetaPotion.DEFAULT_POTION.NBT, this.customPotionTag);
        }
        // MELON END
        
        return builder;
    }

}