package net.minecraft.world.entity.monster;

import java.util.Random;
import java.util.function.Predicate;
import net.minecraft.core.BlockPosition;
import net.minecraft.server.level.WorldServer;
import net.minecraft.sounds.SoundCategory;
import net.minecraft.sounds.SoundEffect;
import net.minecraft.sounds.SoundEffects;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityCreature;
import net.minecraft.world.entity.EntityInsentient;
import net.minecraft.world.entity.EntityLiving;
import net.minecraft.world.entity.EntityTypes;
import net.minecraft.world.entity.EnumMobSpawn;
import net.minecraft.world.entity.ai.attributes.AttributeBase;
import net.minecraft.world.entity.ai.attributes.AttributeProvider;
import net.minecraft.world.entity.ai.attributes.GenericAttributes;
import net.minecraft.world.entity.monster.IMonster;
import net.minecraft.world.entity.player.EntityHuman;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemProjectileWeapon;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.EnumSkyBlock;
import net.minecraft.world.level.GeneratorAccess;
import net.minecraft.world.level.IMaterial;
import net.minecraft.world.level.IWorldReader;
import net.minecraft.world.level.World;
import net.minecraft.world.level.WorldAccess;
import org.bukkit.craftbukkit.v1_18_R1.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_18_R1.entity.CraftMonster;

public abstract class EntityMonster extends EntityCreature implements IMonster {
	public CraftMonster getBukkitMonster() {
		return (CraftMonster) super.getBukkitEntity();
	}

	protected EntityMonster(final EntityTypes<? extends EntityMonster> type, final World world) {
		super((EntityTypes) type, world);
		this.bK = 5;
	}

	public SoundCategory getSoundCategory() {
		return SoundCategory.f;
	}

	public void movementTick() {
		this.ei();
		this.fA();
		super.movementTick();
	}

	protected void fA() {
		final float f = this.aY();
		if (f > 0.5f) {
			this.bf += 2;
		}
	}

	protected boolean Q() {
		return true;
	}

	protected SoundEffect getSoundSwim() {
		return SoundEffects.iN;
	}

	protected SoundEffect getSoundSplash() {
		return SoundEffects.iM;
	}

	protected SoundEffect getSoundHurt(final DamageSource source) {
		return SoundEffects.iK;
	}

	protected SoundEffect getSoundDeath() {
		return SoundEffects.iJ;
	}

	protected SoundEffect getSoundFall(final int distance) {
		return (distance > 4) ? SoundEffects.iI : SoundEffects.iL;
	}

	public float a(final BlockPosition pos, final IWorldReader world) {
		return 0.5f - world.z(pos);
	}

	public static boolean a(final WorldAccess world, final BlockPosition pos, final Random random) {
		if (world.getBrightness(EnumSkyBlock.a, pos) > random.nextInt(32)) {
			return false;
		}
        int i = world.getLevel().Y()
                ? world.c(pos, 10)
                : world.getLightLevel(pos);
        // -- MELON END
        if (i > 5 && world.getMinecraftWorld().getWorld().getName().equals("shadowrealm")) {
            i = 5;
        }
        // -- MELON END
		return i <= random.nextInt(8);
	}

	public static boolean b(final EntityTypes<? extends EntityMonster> type, final WorldAccess world,
			final EnumMobSpawn spawnReason, final BlockPosition pos, final Random random) {
		return world.getDifficulty() != EnumDifficulty.a && a(world, pos, random)
				&& a((EntityTypes) type, (GeneratorAccess) world, spawnReason, pos, random);
	}

	public static boolean c(final EntityTypes<? extends EntityMonster> type, final GeneratorAccess world,
			final EnumMobSpawn spawnReason, final BlockPosition pos, final Random random) {
		return world.getDifficulty() != EnumDifficulty.a && a((EntityTypes) type, world, spawnReason, pos, random);
	}

	public static AttributeProvider.Builder fB() {
		return EntityInsentient.w().a(GenericAttributes.f);
	}

	protected boolean isDropExperience() {
		return true;
	}

	protected boolean dD() {
		return true;
	}

	public boolean f(final EntityHuman player) {
		return true;
	}

	public ItemStack h(final ItemStack stack) {
		if (stack.getItem() instanceof ItemProjectileWeapon) {
			final Predicate<ItemStack> predicate = (Predicate<ItemStack>) ((ItemProjectileWeapon) stack.getItem()).e();
			final ItemStack itemStack = ItemProjectileWeapon.a((EntityLiving) this, (Predicate) predicate);
			return itemStack.isEmpty() ? new ItemStack((IMaterial) Items.mh) : itemStack;
		}
		return ItemStack.b;
	}
}