package net.minecraft.world.entity.animal;

import java.util.Objects;
import java.util.Random;
import java.util.function.Predicate;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_18_R1.inventory.CraftItemStack;
import org.bukkit.entity.Fish;
import org.bukkit.entity.Player;

import net.minecraft.advancements.CriterionTriggers;
import net.minecraft.core.BlockPosition;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.syncher.DataWatcher;
import net.minecraft.network.syncher.DataWatcherObject;
import net.minecraft.network.syncher.DataWatcherRegistry;
import net.minecraft.server.level.EntityPlayer;
import net.minecraft.sounds.SoundEffect;
import net.minecraft.sounds.SoundEffects;
import net.minecraft.tags.Tag;
import net.minecraft.tags.TagsFluid;
import net.minecraft.util.MathHelper;
import net.minecraft.world.EnumHand;
import net.minecraft.world.EnumInteractionResult;
import net.minecraft.world.entity.EntityCreature;
import net.minecraft.world.entity.EntityInsentient;
import net.minecraft.world.entity.EntityLiving;
import net.minecraft.world.entity.EntityPose;
import net.minecraft.world.entity.EntitySize;
import net.minecraft.world.entity.EntityTypes;
import net.minecraft.world.entity.EnumMobSpawn;
import net.minecraft.world.entity.EnumMoveType;
import net.minecraft.world.entity.IEntitySelector;
import net.minecraft.world.entity.ai.attributes.AttributeProvider;
import net.minecraft.world.entity.ai.attributes.GenericAttributes;
import net.minecraft.world.entity.ai.control.ControllerMove;
import net.minecraft.world.entity.ai.goal.PathfinderGoalAvoidTarget;
import net.minecraft.world.entity.ai.goal.PathfinderGoalPanic;
import net.minecraft.world.entity.ai.goal.PathfinderGoalRandomSwim;
import net.minecraft.world.entity.ai.goal.PathfinderGoalSelector;
import net.minecraft.world.entity.ai.navigation.NavigationAbstract;
import net.minecraft.world.entity.ai.navigation.NavigationGuardian;
import net.minecraft.world.entity.player.EntityHuman;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.GeneratorAccess;
import net.minecraft.world.level.World;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.IBlockData;
import net.minecraft.world.phys.Vec3D;
import projectz.listeners.events.PlayerBucketFishEvent;

public abstract class EntityFish extends EntityWaterAnimal implements Bucketable {
	private static final DataWatcherObject<Boolean> b = DataWatcher.a(EntityFish.class, DataWatcherRegistry.i);

	public EntityFish(final EntityTypes<? extends EntityFish> type, final World world) {
		super(type, world);
		this.bM = new a(this);
	}

	protected float b(final EntityPose pose, final EntitySize dimensions) {
		return dimensions.b * 0.65f;
	}

	public static AttributeProvider.Builder n() {
		return EntityInsentient.w().a(GenericAttributes.a, 3.0);
	}

	public boolean isSpecialPersistence() {
		return super.isSpecialPersistence() || this.isFromBucket();
	}

	public static boolean b(final EntityTypes<? extends EntityFish> type, final GeneratorAccess world,
			final EnumMobSpawn spawnReason, final BlockPosition pos, final Random random) {
		return world.getType(pos).a(Blocks.A) && world.getType(pos.up()).a(Blocks.A);
	}

	public boolean isTypeNotPersistent(final double distanceSquared) {
		return true;
	}

	public int getMaxSpawnGroup() {
		return 8;
	}

	protected void initDatawatcher() {
		super.initDatawatcher();
		this.Y.register(EntityFish.b, false);
	}

	public boolean isFromBucket() {
		return (boolean) this.Y.get( EntityFish.b);
	}

	public void setFromBucket(final boolean fromBucket) {
		this.Y.set(EntityFish.b, fromBucket);
		this.setPersistenceRequired(this.isPersistent());
	}

	public void saveData(final NBTTagCompound nbt) {
		super.saveData(nbt);
		nbt.setBoolean("FromBucket", this.isFromBucket());
	}

	public void loadData(final NBTTagCompound nbt) {
		super.loadData(nbt);
		this.setFromBucket(nbt.getBoolean("FromBucket"));
	}

	protected void initPathfinder() {
		super.initPathfinder();
		this.bP.a(0, new PathfinderGoalPanic(this, 1.25));
		final PathfinderGoalSelector pathfindergoalselector = this.bP;
		final Predicate predicate = IEntitySelector.f;
		Objects.requireNonNull(predicate);
		final PathfinderGoalSelector pathfinderGoalSelector = pathfindergoalselector;
		final int n = 2;
		final Class<EntityHuman> clazz = EntityHuman.class;
		final float n2 = 8.0f;
		final double n3 = 1.6;
		final double n4 = 1.4;
		final Predicate obj = predicate;
		Objects.requireNonNull(obj);
		pathfinderGoalSelector.a(n, new PathfinderGoalAvoidTarget(this, clazz, n2, n3, n4, obj::test));
		this.bP.a(4, new b(this));
	}

	protected NavigationAbstract a(final World world) {
		return (NavigationAbstract) new NavigationGuardian((EntityInsentient) this, world);
	}

	public void g(final Vec3D movementInput) {
		if (this.doAITick() && this.isInWater()) {
			this.a(0.01f, movementInput);
			this.move(EnumMoveType.a, this.getMot());
			this.setMot(this.getMot().a(0.9));
			if (this.getGoalTarget() == null) {
				this.setMot(this.getMot().add(0.0, -0.005, 0.0));
			}
		} else {
			super.g(movementInput);
		}
	}

	public void movementTick() {
		if (!this.isInWater() && this.z &&this.B) {
			this.setMot(this.getMot().add((double) (((this).Q.nextFloat() * 2.0f - 1.0f) * 0.05f),
					0.4000000059604645, (double) (((this).Q.nextFloat() * 2.0f - 1.0f) * 0.05f)));
			(this).z = false;
			(this).af = true;
			this.playSound(this.getSoundFlop(), this.getSoundVolume(), this.ep());
		}
		super.movementTick();
	}

	// -- MELON START
    protected EnumInteractionResult b(EntityHuman entityhuman, EnumHand enumhand) {
        ItemStack itemstack = entityhuman.b(enumhand);
        if (itemstack.getItem() == Items.nX && this.isAlive()) {
            ItemStack itemstack1 = this.getBucketItem();
            
            PlayerBucketFishEvent event =
                    new PlayerBucketFishEvent((Player) entityhuman.getBukkitEntity(),
                            (Fish) this.getBukkitEntity(),
                            itemstack.getBukkitStack(),
                            itemstack1.getBukkitStack());
            Bukkit.getPluginManager().callEvent(event);
            
            if (event.isCancelled()) {
                return super.b(entityhuman, enumhand);
            }
            
            itemstack1 = CraftItemStack.asNMSCopy(event.getItemTo());
            this.playSound(((Bucketable) this).t(), 1.0f, 1.0f);
            itemstack.subtract(1);
            this.setBucketName(itemstack1);
            if (!this.t.y) {
                CriterionTriggers.j.a((EntityPlayer) entityhuman, itemstack1);
            }
            if (itemstack.isEmpty()) {
                entityhuman.a(enumhand, itemstack1);
            } else if (!entityhuman.getInventory().pickup(itemstack1)) {
                entityhuman.drop(itemstack1, false);
            }
            this.die();
            return EnumInteractionResult.a(t.y);
        }
        return super.b(entityhuman, enumhand);
    }
    // -- MELON END
    
	public void setBucketName(final ItemStack stack) {
		Bucketable.a((EntityInsentient) this, stack);
	}

	public void c(final NBTTagCompound nbt) {
		Bucketable.a((EntityInsentient) this, nbt);
	}

	public SoundEffect t() {
		return SoundEffects.ca;
	}

	protected boolean fw() {
		return true;
	}

	protected abstract SoundEffect getSoundFlop();

	protected SoundEffect getSoundSwim() {
		return SoundEffects.gd;
	}

	protected void b(final BlockPosition pos, final IBlockData state) {
	}

	private static class a extends ControllerMove {
		private final EntityFish l;

		a(final EntityFish owner) {
			super((EntityInsentient) owner);
			this.l = owner;
		}

		public void a() {
			if (this.l.a((Tag) TagsFluid.b)) {
				this.l.setMot(this.l.getMot().add(0.0, 0.005, 0.0));
			}
			if (super.k == ControllerMove.Operation.b && !this.l.getNavigation().m()) {
				final float f = (float) (super.h * this.l.b(GenericAttributes.d));
				this.l.r(MathHelper.h(0.125f, this.l.ew(), f));
				final double d0 = super.e - this.l.locX();
				final double d2 = super.f - this.l.locY();
				final double d3 = super.g - this.l.locZ();
				if (d2 != 0.0) {
					final double d4 = Math.sqrt(d0 * d0 + d2 * d2 + d3 * d3);
					this.l.setMot(this.l.getMot().add(0.0, this.l.ew() * (d2 / d4) * 0.1, 0.0));
				}
				if (d0 != 0.0 || d3 != 0.0) {
					final float f2 = (float) (MathHelper.d(d3, d0) * 57.2957763671875) - 90.0f;
					this.l.setYRot(this.a(this.l.getYRot(), f2, 90.0f));
					((EntityLiving) this.l).aX = this.l.getYRot();
				}
			} else {
				this.l.r(0.0f);
			}
		}
	}

	private static class b extends PathfinderGoalRandomSwim {
		private final EntityFish i;

		public b(final EntityFish fish) {
			super((EntityCreature) fish, 1.0, 40);
			this.i = fish;
		}

		public boolean a() {
			return this.i.fw() && super.a();
		}
	}
}