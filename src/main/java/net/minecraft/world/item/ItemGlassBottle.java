package net.minecraft.world.item;

import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_18_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;

import net.minecraft.core.BlockPosition;
import net.minecraft.sounds.SoundCategory;
import net.minecraft.sounds.SoundEffects;
import net.minecraft.stats.StatisticList;
import net.minecraft.tags.Tag;
import net.minecraft.tags.TagsFluid;
import net.minecraft.world.EnumHand;
import net.minecraft.world.InteractionResultWrapper;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityAreaEffectCloud;
import net.minecraft.world.entity.boss.enderdragon.EntityEnderDragon;
import net.minecraft.world.entity.player.EntityHuman;
import net.minecraft.world.item.alchemy.PotionRegistry;
import net.minecraft.world.item.alchemy.PotionUtil;
import net.minecraft.world.item.alchemy.Potions;
import net.minecraft.world.level.IMaterial;
import net.minecraft.world.level.RayTrace;
import net.minecraft.world.level.World;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.MovingObjectPosition;
import net.minecraft.world.phys.MovingObjectPositionBlock;

import projectz.listeners.events.BottleFillEvent;

public class ItemGlassBottle extends Item {

	public ItemGlassBottle(final Item.Info settings) {
		super(settings);
	}

	@Override
	public InteractionResultWrapper<ItemStack> a(final World world, final EntityHuman user, final EnumHand hand) {
		final List<EntityAreaEffectCloud> list = (List<EntityAreaEffectCloud>) world.a(
				EntityAreaEffectCloud.class, user.getBoundingBox().g(2.0),
				entity -> entity != null && entity.isAlive() && entity.getSource() instanceof EntityEnderDragon);
		final ItemStack itemStack = user.b(hand);
		if (!list.isEmpty()) {
			final EntityAreaEffectCloud areaEffectCloud = list.get(0);
			areaEffectCloud.setRadius(areaEffectCloud.getRadius() - 0.5f);
			world.playSound((EntityHuman) null, user.locX(), user.locY(), user.locZ(), SoundEffects.bM, SoundCategory.g,
					1.0f, 1.0f);
			world.a((Entity) user, GameEvent.z, user.getChunkCoordinates());
			return (InteractionResultWrapper<ItemStack>) InteractionResultWrapper
					.a(this.a(itemStack, user, new ItemStack((IMaterial) Items.sq)), world.isClientSide());
		}
		final MovingObjectPosition hitResult = (MovingObjectPosition) a(world, user, RayTrace.FluidCollisionOption.b);
		if (hitResult.getType() == MovingObjectPosition.EnumMovingObjectType.a) {
			return (InteractionResultWrapper<ItemStack>) InteractionResultWrapper.pass(itemStack);
		}
		if (hitResult.getType() == MovingObjectPosition.EnumMovingObjectType.b) {
			final BlockPosition blockPos = ((MovingObjectPositionBlock) hitResult).getBlockPosition();
			if (!world.a(user, blockPos)) {
				return (InteractionResultWrapper<ItemStack>) InteractionResultWrapper.pass(itemStack);
			}
			if (world.getFluid(blockPos).a((Tag) TagsFluid.b)) {	
    			// -- MELON START
    			ItemStack to = PotionUtil.a(new ItemStack((IMaterial) Items.pF), (PotionRegistry) Potions.b);

    			BottleFillEvent event = new BottleFillEvent((Player) user.getBukkitEntity(), itemStack.getBukkitStack(), to.getBukkitStack(), world.getWorld().getBlockAt(blockPos.getX(), blockPos.getY(), blockPos.getZ()));
    			Bukkit.getPluginManager().callEvent(event);

    			if (event.isCancelled()) {
    				return InteractionResultWrapper.fail(itemStack);
    			}

    			world.playSound(user, user.locX(), user.locY(), user.locZ(), SoundEffects.bL, SoundCategory.g, 1.0F, 1.0F);
    			return InteractionResultWrapper.success(this.a(itemStack, user,
    					CraftItemStack.asNMSCopy(event.getItemTo())));
    			// -- MELON END

    			//return InteractionResultWrapper.a(this.a(itemstack, entityhuman, PotionUtil.a(new ItemStack(Items.POTION), Potions.WATER)), world.s_());
    		}
    	}

    	return InteractionResultWrapper.pass(itemStack);
    }

	protected ItemStack a(final ItemStack stack, final EntityHuman player, final ItemStack outputStack) {
		player.b(StatisticList.c.b(this));
		return ItemLiquidUtil.a(stack, player, outputStack);
	}
}
