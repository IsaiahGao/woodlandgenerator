package net.minecraft.world.level.block;

import java.util.Random;
import net.minecraft.core.BaseBlockPosition;
import net.minecraft.core.BlockPosition;
import net.minecraft.core.EnumDirection;
import net.minecraft.server.level.WorldServer;
import net.minecraft.util.MathHelper;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.IBlockAccess;
import net.minecraft.world.level.World;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.BlockIce;
import net.minecraft.world.level.block.state.BlockBase;
import net.minecraft.world.level.block.state.BlockStateList;
import net.minecraft.world.level.block.state.IBlockData;
import net.minecraft.world.level.block.state.BlockBase.Info;
import net.minecraft.world.level.block.state.properties.BlockProperties;
import net.minecraft.world.level.block.state.properties.BlockStateInteger;
import net.minecraft.world.level.block.state.properties.IBlockState;

public class BlockIceFrost extends BlockIce {
	public static final int a = 3;
	public static final BlockStateInteger b;
	private static final int c = 4;
	private static final int d = 2;

	public BlockIceFrost(final BlockBase.Info settings) {
		super(settings);
		this.k((IBlockData) ((IBlockData) ((Block) this).E.getBlockData()).set((IBlockState) BlockIceFrost.b,
				(Comparable) 0));
	}

	public void tick(final IBlockData state, final WorldServer world, final BlockPosition pos, final Random random) {
		this.tickAlways(state, world, pos, random);
	}

	public void tickAlways(final IBlockData state, final WorldServer world, final BlockPosition pos,
			final Random random) {
		if (!((World) world).paperConfig.frostedIceEnabled) {
			return;
		}
		if ((random.nextInt(3) == 0 || this.a((IBlockAccess) world, pos, 4))
				// --- MELON START
				&& (world.getWorld().getName().equals("shadowrealm") ? 14 : world.getLightLevel(pos)) > 11
				// --- MELON END
				- (int) state.get((IBlockState) BlockIceFrost.b) - state.b((IBlockAccess) world, pos)
				&& this.e(state, (World) world, pos)) {
			final BlockPosition.MutableBlockPosition mutableBlockPos = new BlockPosition.MutableBlockPosition();
			for (final EnumDirection direction : EnumDirection.values()) {
				mutableBlockPos.a((BaseBlockPosition) pos, direction);
				final IBlockData blockState = world.getTypeIfLoaded((BlockPosition) mutableBlockPos);
				if (blockState != null) {
					if (blockState.a((Block) this)
							&& !this.e(blockState, (World) world, (BlockPosition) mutableBlockPos)) {
						world.getBlockTickList().a((BlockPosition) mutableBlockPos, this,
								MathHelper.nextInt(random, ((World) world).paperConfig.frostedIceDelayMin,
										((World) world).paperConfig.frostedIceDelayMax));
					}
				}
			}
		} else {
			world.getBlockTickList().a(pos, this, MathHelper.nextInt(random,
					((World) world).paperConfig.frostedIceDelayMin, ((World) world).paperConfig.frostedIceDelayMax));
		}
	}

	private boolean e(final IBlockData state, final World world, final BlockPosition pos) {
		final int i = (int) state.get((IBlockState) BlockIceFrost.b);
		if (i < 3) {
			world.setTypeAndData(pos, (IBlockData) state.set((IBlockState) BlockIceFrost.b, (Comparable) (i + 1)), 2);
			return false;
		}
		this.melt(state, world, pos);
		return true;
	}

	public void doPhysics(final IBlockData state, final World world, final BlockPosition pos, final Block block,
			final BlockPosition fromPos, final boolean notify) {
		if (block.getBlockData().a((Block) this) && this.a((IBlockAccess) world, pos, 2)) {
			this.melt(state, world, pos);
		}
		super.doPhysics(state, world, pos, block, fromPos, notify);
	}

	private boolean a(final IBlockAccess world, final BlockPosition pos, final int maxNeighbors) {
		int i = 0;
		final BlockPosition.MutableBlockPosition mutableBlockPos = new BlockPosition.MutableBlockPosition();
		for (final EnumDirection direction : EnumDirection.values()) {
			mutableBlockPos.a((BaseBlockPosition) pos, direction);
			final IBlockData blockState = world.getTypeIfLoaded((BlockPosition) mutableBlockPos);
			if (blockState != null && blockState.a((Block) this) && ++i >= maxNeighbors) {
				return false;
			}
		}
		return true;
	}

	protected void a(final BlockStateList.a<Block, IBlockData> builder) {
		builder.a(new IBlockState[]{(IBlockState) BlockIceFrost.b});
	}

	public ItemStack a(final IBlockAccess world, final BlockPosition pos, final IBlockData state) {
		return ItemStack.b;
	}

	static {
		b = BlockProperties.ao;
	}
}